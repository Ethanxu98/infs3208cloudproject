<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/stylesheet.css">
    <title>Search Results</title>
  </head>
  <body>
    <header>
      <?php
        include 'header.php';
      ?>
    </header>
    <div class="container-fluid" id="main">
      <div class="row justify-content-center">
        <div class="col-lg-auto">
          <div class="d-flex flex-column justify-content-lg-center">
            <div class="row-fluid" style="text-align: center;">
                <ul class="list-group list-group-flush category-titles">
                    <li class="list-group-item"><b><?php echo $_GET['categorySearch']; ?></b></li>
                </ul>
            </div>
            <div class="list-group">
                <a href="#" class="list-group-item list-group-item-action">First item</a>
                <a href="#" class="list-group-item list-group-item-action">Second item</a>
                <a href="#" class="list-group-item list-group-item-action">Third item</a>
                <a href="#" class="list-group-item list-group-item-action">Third item</a>
                <a href="#" class="list-group-item list-group-item-action">Third item</a>
                <a href="#" class="list-group-item list-group-item-action">Third item</a>
            </div>
          </div>
        </div>
        <div class="col-lg-7">
          <div class="d-flex flex-wrap justify-content-start">
          <?php foreach ($searchData->result() as $row): ?>
            <?php 
            if ($row->saleImage1 != ''){
              $saleThumbNail = $row->saleImage1;
            } else {
              $saleThumbNail = base_url().'assets/images/placeholder-images-image_large.png';
            }
            echo
              '<div class="d-flex flex-column justify-content-center align-content-around sale-item">
                <div class="pt-2 pr-3"><img src="'.$saleThumbNail.'" class="placeholder-items-img"></div>
                <div class="d-flex flex-column flex-nowrap justify-content-between">
                  <a href="'.base_url().'item/itemID/'.$row->saleID.' " class="searchItemLink">'.$row->saleName.'</a>
                  <div class="d-flex flex-row star-rating justify-content-center mt-2 mb-2">
                    <img src="'.base_url().'assets/images/star.png" style="width: 20px;">
                    <img src="'.base_url().'assets/images/star.png" style="width: 20px;">
                    <img src="'.base_url().'assets/images/star.png" style="width: 20px;">
                  </div>
                  <p style="font-size:16px;">$'.$row->salePrice.'</p>
                </div>
              </div>'
            ?>
          <?php endforeach; ?>

          </div>
        </div>
      </div>
      <div class="row justify-content-center pt-5">
        <h2>Recently viewed items</h2>
      </div>
      <div class="row justify-content-center" style="background-color: grey;">
        <div class="col-md-auto">
          <div class="d-flex flex-wrap">
            <img src="<?php echo base_url();?>assets/images/placeholder-images-image_large.png" class="popular-categories">
            <img src="<?php echo base_url();?>assets/images/placeholder-images-image_large.png" class="popular-categories">
            <img src="<?php echo base_url();?>assets/images/placeholder-images-image_large.png" class="popular-categories">
            <img src="<?php echo base_url();?>assets/images/placeholder-images-image_large.png" class="popular-categories">
          </div>
        </div>
      </div>
    </div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
  </body>
</html>