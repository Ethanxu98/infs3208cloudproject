<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/stylesheet.css">
    <title>Login</title>
  </head>
  <body>
    <header>
      <?php
        include 'header.php';
      ?>
    </header>
    <div class="container-fluid" id="main">
        <div class="row justify-content-center" style="padding-top: 5%;">
          <h2>Returning user</h2>
        </div>
        <div class="row justify-content-center">
          <div class="col-4">
            <?php
            if ($this->session->flashdata('message')){
              echo '<div class="alert alert-success">'.$this->session->flashdata("message").'</div>';
            }
            ?>
            <form method="POST" action="<?php echo base_url();?>login/validation" id="login-form">
              <div class="form-group">
                <label for="email">Email address:</label>
                <input type="email" class="form-control" placeholder="Enter email" id="email" name="email" value="<?php 
                if (get_cookie('rememberedEmail')){
                  echo get_cookie('rememberedEmail');}?>">
                <span class="text-danger"><?php echo form_error('email');?></span>
              </div>
              <div class="form-group">
                <label for="pwd">Password:</label>
                <input type="password" class="form-control" placeholder="Enter password" id="pwd" name="pwd" value="<?php 
                if (get_cookie('rememberedPwd')){
                  echo get_cookie('rememberedPwd');}?>">
                <span class="text-danger"><?php echo form_error('pwd');?></span>
              </div>
              <div class="row">
                <div class="form-group form-check" style="left: 15px; text-align: left; width: 50%;">
                  <label class="form-check-label" >
                    <input class="form-check-input" type="checkbox" name="autoLogin" <?php if (get_cookie('rememberedEmail')) { ?> checked="checked" <?php } ?>> Remember me
                  </label>
                </div>
                <div class="form-group form-check" style="right: 15px; text-align: right; width: 50%;">
                  <p>New here? <a href="<?php echo base_url();?>register">Signup</a></p>
                </div>
                <div class="form-group form-check">
                  <p><a href="<?php echo base_url();?>login/resetPassword">Forgot Password?</a></p>
                </div>
              </div>
              <button type="submit" class="btn btn-primary" name="login">Submit</button>
            </form>
          </div>
        </div>
    </div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    
  </body>
</html>