<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/stylesheet.css">
    <title>My account</title>
  </head>
  <body>
    <header>
      <?php
        include 'header.php';
      ?>
    </header>
    <div class="container-fluid" id="main">
        <div class="row justify-content-center mt-5">
            <div class="col-lg-2">
                <div class="d-flex flex-column justify-content-lg-center">
                    <div>
                        <ul class="list-group list-group-flush">
                        <li class="list-group-item"><a class="list-group-item" href="<?php echo base_url();?>account">Account Settings</a></li>
                        <li class="list-group-item"><a class="list-group-item" href="#">Watch List</a></li>
                        <li class="list-group-item"><a class="list-group-item" href="#">My Orders</a></li>
                        <li class="list-group-item"><a class="list-group-item" href="#">My Sales</a></li>
                        <li class="list-group-item"><a class="list-group-item" href="#">My Bids</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-lg-5">
                <div class="row justify-content-center">
                    <div class="col-12">
                        <?php
                            if ($this->session->flashdata('message')){
                                echo '<div class="alert alert-success">'.$this->session->flashdata("message").'</div>';
                            }
                            if ($accDetails->userVerified == TRUE){
                                $verified = 'Yes';
                            } else {
                                $verified = 'No';
                            }
                        ?>
                        <form method="POST" action="<?php echo base_url();?>account/updateAccountInfo" id="userProfileForm">
                        <div class="form-group">
                            <label for="email">Email address:</label>
                            <input disabled type="email" class="form-control" placeholder="Enter email" id="email" name="email" value="<?php print_r($accDetails->emailID);?>">
                            <span class="text-danger"><?php echo form_error('email');?></span>
                        </div>
                        <div class="row justify-content-center">
                            <div class="col-lg-6">
                            <div class="form-group">
                                <label for="username-firstname">First Name</label>
                                <input type="username-firstname" class="form-control" placeholder="Name" id="userFName" name="userFName" value="<?php print_r($accDetails->userFName);?>">
                                <span class="text-danger"><?php echo form_error('userFName');?></span>
                            </div>
                            </div>
                            <div class="col-lg-6">
                            <div class="form-group">
                                <label for="username-surname">Last Name</label>
                                <input type="username-surname" class="form-control" placeholder="Name" id="userLName" name="userLName" value="<?php print_r($accDetails->userLName);?>">
                                <span class="text-danger"><?php echo form_error('userLName');?></span>
                            </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="address1">Street Address:</label>
                            <input type="address" class="form-control" placeholder="Address Line 1" id="address1" name="address1" value="<?php print_r($accDetails->StreetAddress);?>">
                            <span class="text-danger"><?php echo form_error('address1');?></span>
                        </div>
                        <div class="form-group">
                            <label for="address2">Address Line 2</label>
                            <input type="address" class="form-control" placeholder="Address Line 2" id="address2" name="address2" value="<?php print_r($accDetails->AddressLine2);?>">
                        </div>
                        <div class="row justify-content-center">
                            <div class="col-lg-6">
                            <div class="form-group">
                                <label for="city">City/Suburb</label>
                                <input type="city" class="form-control" placeholder="Name" id="city" name="city" value="<?php print_r($accDetails->City);?>">
                                <span class="text-danger"><?php echo form_error('city');?></span>
                            </div>
                            </div>
                            <div class="col-lg-6">
                            <div class="form-group">
                                <label for="state">State/Province</label>
                                <input type="state" class="form-control" placeholder="Name" id="state" name="state" value="<?php print_r($accDetails->State);?>">
                                <span class="text-danger"><?php echo form_error('state');?></span>
                            </div>
                            </div>
                        </div>
                        <div class="row justify-content-center">
                            <div class="col-lg-6">
                            <div class="form-group">
                                <label for="postcode">Postcode</label>
                                <input type="postcode" class="form-control" placeholder="Postcode" id="postcode" name="postcode" value="<?php print_r($accDetails->PostCode);?>">
                            </div>
                            </div>
                            <div class="col-lg-6">
                            <div class="form-group">
                                <label for="country">Country</label>
                                <input type="country" class="form-control" placeholder="Name" id="country" name="country" value="<?php print_r($accDetails->Country);?>">
                                <span class="text-danger"><?php echo form_error('country');?></span>
                            </div>
                            </div>
                            <p>Email verified: <?php echo $verified?></p>
                        </div>
                        <div class="row">
                            <div class="form-group form-check" style="text-align: left; width: 50%;">
                            <p>Existing user? <a href="<?php echo base_url();?>">Login</a></p>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary" name="register">Save</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    
  </body>
</html>