<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/stylesheet.css">
    <title>Forgot Password</title>
  </head>
  <body>
    <header>
      <?php
        include 'header.php';
      ?>
    </header>
    <div class="container-fluid" id="main">
        <div class="row justify-content-center" style="padding-top: 5%;">
          <h2>Reset Password</h2>
        </div>
        <div class="row justify-content-center">
          <div class="col-4">
            <?php
            if ($this->session->flashdata('message')){
              echo '<div class="alert alert-success">'.$this->session->flashdata("message").'</div>';
            }
            ?>
            <form method="POST" action="<?php echo base_url();?>login/resetPassword" id="login-form">
              <div class="form-group">
                <label for="email">Email address:</label>
                <input type="email" class="form-control" placeholder="Enter email" id="email" name="email">
                <span class="text-danger"><?php echo form_error('email');?></span>
              </div>
              <a href="<?php echo base_url();?>login" class="btn btn-primary">Back</a>
              <button type="submit" class="btn btn-primary" name="submit">Submit</button>
            </form>
          </div>
        </div>
    </div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    
  </body>
</html>