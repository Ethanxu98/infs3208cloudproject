<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/stylesheet.css">

    <!-- Automatically logs user out after 30 minutes of inactivity -->
    <?php
      $minutes = 30;
      $isLoggedIn = $this->session->userdata('logged_in');
      if(isset($_SESSION['time']) && (time()-$_SESSION['time'] > $minutes*60)){
        header('location:'. ''.base_url().'home/logout');
        die();
      }
      if($isLoggedIn){ $_SESSION['time'] = time(); } //sets time since user logged in
    ?>

    <title>Online Auction</title>
  </head>
  <body>
        <nav id="header-nav">
            <div class="row justify-content-lg-center pt-4">
              <div class="col-lg-2"></div>
              <div class="col-lg-8" style="text-align:center;">
                <h1><a href="<?php echo base_url();?>" style="text-decoration: none; color: black; ">Online auction</a></h1>
              </div>
              <div class="col-lg-2">
                <div class="row justify-content-center">
                  <div class="col-auto">
                    <h1>Cart</h1>
                  </div>
                  <div class="col-auto">
                    <div class="d-flex justify-content-sm-center">
                        <p class="col-auto">Welcome <?php echo $this->session->userdata('userFName');?></p>
                    </div>
                    <div class="d-flex flex-row justify-content-center">
                        <?php
                            if($this->session->userdata('logged_in') != TRUE){
                              echo '<a href="'.base_url().'login" class="col-auto btn btn-primary mr-1">Login</a>
                                <a href="'.base_url().'register" class="col-auto btn btn-primary mr-1">Register</a>';
                            }
                            else {
                              echo '<form action="'.base_url().'home/logout" method="POST"><button type="submit" class="col-auto btn btn-primary mr-1" name="logout-submit">Logout</button></form>';
                              echo '<form action="'.base_url().'account" method="POST"><button type="submit" class="col-auto btn btn-primary mr-1" name="logout-submit">My Account</button></form>';
                            }
                        ?>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="row-fluid justify-content-center align-content-center pt-4">
              <form method="GET" action="<?php echo base_url();?>home/search" id="searchForm">
                <div class="d-flex flex-row justify-content-center align-items-center pt-4">
                  <div class="col-lg-8">
                    <?php
                      if (isset($_GET['searchTerm']))
                      {
                        $term = $_GET['searchTerm'];
                      } else {
                        $term = "";
                      }
                      if (isset($_GET['categorySearch'])){
                        $categorySearch = 'selected';
                      } else {
                        $categorySearch = '';
                      }
                    ?>
                    <div class="form-group">
                      <input type="text" name="searchTerm" id="searchTerm" class="form-control form-control typeahead" placeholder="Search items" value="<?php echo $term;?>">
                    </div>
                  </div>
                  <div class="col-auto form-group">
                    <select class="form-control" name="categorySearch" id="categorySearch">
                      <option class="dropdown-item" value="All">All</option>
                      <option class="dropdown-item" value="Antiques">Antiques</option>
                      <option class="dropdown-item" value="Art">Art</option>
                      <option class="dropdown-item" value="Clothing Fashion">Clothing & Fashion</option>
                      <option class="dropdown-item" value="Digital Entertainment">Digital Entertainment</option>
                      <option class="dropdown-item" value="Electronics">Electronics</option>
                      <option class="dropdown-item" value="Home Appliances">Home Appliances</option>
                      <option class="dropdown-item" value="Toys Hobbies">Toys & Hobbies</option>
                      <option class="dropdown-item" value="Sporting Outdoors">Sporting & Outdoors</option>
                      <option class="dropdown-item" value="Other">Other</option>
                    </select>
                  </div>
                </div>
              </form>
            </div>
        </nav>
        <?php if($this->session->userdata('logged_in') == TRUE){
        echo'<div class="row justify-content-center mt-2">
              <ul class="list-group list-group-horizontal">
                <a class="list-group-item" href="'.base_url().'sell">Sell</a>
                <a class="list-group-item" href="'.base_url().'favourites">Watch List</a>
                <a class="list-group-item" href="'.base_url().'cart">My cart/My bids</a>
              </ul>
            </div>';
        }
        ?>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="<?php echo base_url();?>assets/js/jquery-3.4.1.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url();?>assets/js/jquery.cookie-1.4.1.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url();?>assets/js/header.js" type="text/javascript"></script>
  </body>
</html>
