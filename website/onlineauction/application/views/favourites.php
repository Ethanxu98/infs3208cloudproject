<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/stylesheet.css">
    <title>Watch List</title>
  </head>
  <body>
    <header>
      <?php
        include 'header.php';
      ?>
    </header>
    <div class="container-fluid" id="main">
        <div class="row justify-content-center mt-5">
            <div class="col-lg-2">
                <div class="d-flex flex-column justify-content-lg-center">
                    <div>
                        <ul class="list-group list-group-flush">
                        <li class="list-group-item"><a class="list-group-item" href="<?php echo base_url();?>account">Account Settings</a></li>
                        <li class="list-group-item"><a class="list-group-item" href="#">Watch List</a></li>
                        <li class="list-group-item"><a class="list-group-item" href="#">My Orders</a></li>
                        <li class="list-group-item"><a class="list-group-item" href="#">My Sales</a></li>
                        <li class="list-group-item"><a class="list-group-item" href="#">My Bids</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-lg-5">
                <?php foreach ($favourite->result() as $row): ?>
                <?php
                $currentUser = $this->session->userdata('userID'); 
                if ($row->saleImage1 != ''){
                    $saleThumbNail = $row->saleImage1;
                } else {
                    $saleThumbNail = base_url().'assets/images/placeholder-images-image_large.png';
                }
                echo
                '<div class="row justify-content-center">
                    <div class="col-12 border">
                        <div class="row">
                            <div class="col-5 d-flex justify-content-center">
                                <img src="'.$saleThumbNail.'" class="img-fluid img-thumbnail">
                            </div>
                            <div class="col-7 d-flex flex-column justify-content-around align-items-center">
                                <a href="'.base_url().'item/itemID/'.$row->saleID.'" style="font-size:24px;">'.$row->saleName.'</a>
                                <p>'.$row->saleSubCategory.'</p>
                                <p>'.$row->saleCondition.'</p>
                                <p>'.$row->saleItemDescription.'</p>
                                <h2 style="font-size:24px;">$'.$row->salePrice.'</h2>
                                <a href="'.base_url().'favourites/removeFromFavourite/'.$row->saleID.'/'.$currentUser.'" class="btn btn-primary">Remove From Watch List</a>
                            </div>
                        </div>
                    </div>
                </div>';?>
                <?php endforeach;?>
            </div>
        </div>
    </div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    
  </body>
</html>