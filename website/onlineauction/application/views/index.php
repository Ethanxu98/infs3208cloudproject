<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/stylesheet.css">
    <title>Home</title>
  </head>
  <body>
    <header>
      <?php
        include 'header.php';
      ?>
    </header>
    <div class="container-fluid" id="main">
      <div class="row justify-content-center pt-5">
        <h2>Featured Items</h2>
      </div>
      <div class="row justify-content-center">
        <div class="col-lg-2">
          <div class="d-flex flex-column justify-content-lg-center">
            <div class="btn-group dropright pt-1">
              <button type="button" class="btn btn-primary dropdown-toggle category-btn" data-toggle="dropdown">Antiques</button>
              <ul class="dropdown-menu">
              <li class="dropdown-item"><a class="dropdown-item" href="#">Architectural</a></li>
              <li class="dropdown-item"><a class="dropdown-item" href="#">Decorative Arts</a></li>
              <li class="dropdown-item"><a class="dropdown-item" href="#">Furniture</a></li>
              <li class="dropdown-item"><a class="dropdown-item" href="#">Silver</a></li>
              <li class="dropdown-item"><a class="dropdown-item" href="#">Sculptures</a></li>
              </ul>
            </div>
            <div class="btn-group dropright">
              <button type="button" class="btn btn-primary dropdown-toggle category-btn" data-toggle="dropdown">Art</button>
              <ul class="dropdown-menu">
              <li class="dropdown-item"><a class="dropdown-item" href="#">Indigenous</a></li>
              <li class="dropdown-item"><a class="dropdown-item" href="#">Drawings</a></li>
              <li class="dropdown-item"><a class="dropdown-item" href="#">Photographs</a></li>
              <li class="dropdown-item"><a class="dropdown-item" href="#">Painting</a></li>
              </ul>
            </div>
            <div class="btn-group dropright">
              <button type="button" class="btn btn-primary dropdown-toggle category-btn" data-toggle="dropdown">Clothing & Fashion</button>
              <ul class="dropdown-menu">
              <li class="dropdown-item"><a class="dropdown-item" href="#">Mens' Fashion</a></li>
              <li class="dropdown-item"><a class="dropdown-item" href="#">Womens' Fashion</a></li>
              <li class="dropdown-item"><a class="dropdown-item" href="#">Childrens' Fashion</a></li>
              <li class="dropdown-item"><a class="dropdown-item" href="#">Accessories</a></li>
              <li class="dropdown-item"><a class="dropdown-item" href="#">Costumes</a></li>
              </ul>
            </div>
            <div class="btn-group dropright">
              <button type="button" class="btn btn-primary dropdown-toggle category-btn" data-toggle="dropdown">Digital Entertainment</button>
              <ul class="dropdown-menu">
              <li class="dropdown-item"><a class="dropdown-item" href="#">Video Games</a></li>
              <li class="dropdown-item"><a class="dropdown-item" href="#">DVD, Movies</a></li>
              <li class="dropdown-item"><a class="dropdown-item" href="#">CD, Music</a></li>
              <li class="dropdown-item"><a class="dropdown-item" href="#">Books & Magazines</a></li>
              </ul>
            </div>
            <div class="btn-group dropright">
              <button type="button" class="btn btn-primary dropdown-toggle category-btn" data-toggle="dropdown">Electronics</button>
              <ul class="dropdown-menu">
              <li class="dropdown-item"><a class="dropdown-item" href="#">Pre-built Computers</a></li>
              <li class="dropdown-item"><a class="dropdown-item" href="#">Computer Components</a></li>
              <li class="dropdown-item"><a class="dropdown-item" href="#">Smartphones</a></li>
              <li class="dropdown-item"><a class="dropdown-item" href="#">Cameras</a></li>
              <li class="dropdown-item"><a class="dropdown-item" href="#">Network Hardware</a></li>
              <li class="dropdown-item"><a class="dropdown-item" href="#">Drones</a></li>
              <li class="dropdown-item"><a class="dropdown-item" href="#">Tablets</a></li>
              <li class="dropdown-item"><a class="dropdown-item" href="#">Audio Equipment</a></li>
              <li class="dropdown-item"><a class="dropdown-item" href="#">TVs & Monitors</a></li>
              <li class="dropdown-item"><a class="dropdown-item" href="#">Video Game Consoles</a></li>
              </ul>
            </div>
            <div class="btn-group dropright">
              <button type="button" class="btn btn-primary dropdown-toggle category-btn" data-toggle="dropdown">Home Appliances</button>
              <ul class="dropdown-menu">
              <li class="dropdown-item"><a class="dropdown-item" href="#">Furniture</a></li>
              <li class="dropdown-item"><a class="dropdown-item" href="#">Kitchen</a></li>
              <li class="dropdown-item"><a class="dropdown-item" href="#">Lighting</a></li>
              <li class="dropdown-item"><a class="dropdown-item" href="#">Gardening</a></li>
              <li class="dropdown-item"><a class="dropdown-item" href="#">Home Decor</a></li>
              <li class="dropdown-item"><a class="dropdown-item" href="#">DIY & Materials</a></li>
              </ul>
            </div>
            <div class="btn-group dropright">
              <button type="button" class="btn btn-primary dropdown-toggle category-btn" data-toggle="dropdown">Toys & Hobbies</button>
              <ul class="dropdown-menu">
              <li class="dropdown-item"><a class="dropdown-item" href="#">Action Figures</a></li>
              <li class="dropdown-item"><a class="dropdown-item" href="#">Childrens' Toys</a></li>
              <li class="dropdown-item"><a class="dropdown-item" href="#">Collectible Card Games</a></li>
              <li class="dropdown-item"><a class="dropdown-item" href="#">Model Toys</a></li>
              <li class="dropdown-item"><a class="dropdown-item" href="#">Puzzles</a></li>
              <li class="dropdown-item"><a class="dropdown-item" href="#">Outdoor Toys</a></li>
              </ul>
            </div>
            <div class="btn-group dropright">
              <button type="button" class="btn btn-primary dropdown-toggle category-btn" data-toggle="dropdown">Sporting & Outdoors</button>
              <ul class="dropdown-menu">
              <li class="dropdown-item"><a class="dropdown-item" href="#">Sports Equipment</a></li>
              <li class="dropdown-item"><a class="dropdown-item" href="#">Camping, Hiking</a></li>
              <li class="dropdown-item"><a class="dropdown-item" href="#">Fishing</a></li>
              <li class="dropdown-item"><a class="dropdown-item" href="#">Fitness</a></li>
              </ul>
            </div>
            <div class="btn-group dropright">
              <button type="button" class="btn btn-primary dropdown-toggle category-btn" data-toggle="dropdown">Other</button>
              <ul class="dropdown-menu">
              <li class="dropdown-item"><a class="dropdown-item" href="#">Miscellaneous</a></li>
              </ul>
            </div>
          </div>
        </div>
        <div class="col-lg-7">
          <div class="row justify-content-center">
            <div class="d-flex">
              <div class="pt-1 pr-4 ml-3"><img src="<?php echo base_url();?>assets/images/placeholder-images-image_large.png" class="placeholder-items-img"></div>
              <div class="pt-1 pr-4"><img src="<?php echo base_url();?>assets/images/placeholder-images-image_large.png" class="placeholder-items-img"></div>
              <div class="pt-1 pr-4"><img src="<?php echo base_url();?>assets/images/placeholder-images-image_large.png" class="placeholder-items-img"></div>
            </div>
            <div class="d-flex">
              <div class="pt-2 pb-2 pr-4 ml-3"><img src="<?php echo base_url();?>assets/images/placeholder-images-image_large.png" class="placeholder-items-img"></div>
              <div class="pt-2 pb-2 pr-4"><img src="<?php echo base_url();?>assets/images/placeholder-images-image_large.png" class="placeholder-items-img"></div>
              <div class="pt-2 pb-2 pr-4"><img src="<?php echo base_url();?>assets/images/placeholder-images-image_large.png" class="placeholder-items-img"></div>
            </div>
          </div>
        </div>
      </div>
      <div class="row justify-content-center pt-5">
        <h2>Popular Categories</h2>
      </div>
      <div class="row justify-content-center" style="background-color: grey;">
        <div class="col-md-auto">
          <div class="d-flex flex-wrap">
            <img src="<?php echo base_url();?>assets/images/placeholder-images-image_large.png" class="popular-categories">
            <img src="<?php echo base_url();?>assets/images/placeholder-images-image_large.png" class="popular-categories">
            <img src="<?php echo base_url();?>assets/images/placeholder-images-image_large.png" class="popular-categories">
            <img src="<?php echo base_url();?>assets/images/placeholder-images-image_large.png" class="popular-categories">
          </div>
        </div>
      </div>
      <div class="row justify-content-center" style="background-color: grey;">
        <div class="col-md-auto">
          <div class="d-flex flex-wrap">
            <img src="<?php echo base_url();?>assets/images/placeholder-images-image_large.png" class="popular-categories">
            <img src="<?php echo base_url();?>assets/images/placeholder-images-image_large.png" class="popular-categories">
            <img src="<?php echo base_url();?>assets/images/placeholder-images-image_large.png" class="popular-categories">
            <img src="<?php echo base_url();?>assets/images/placeholder-images-image_large.png" class="popular-categories">
          </div>
        </div>
      </div>
      <div class="row justify-content-center" style="background-color: grey;">
        <div class="col-md-auto">
          <div class="d-flex flex-wrap">
            <img src="<?php echo base_url();?>assets/images/placeholder-images-image_large.png" class="popular-categories">
            <img src="<?php echo base_url();?>assets/images/placeholder-images-image_large.png" class="popular-categories">
            <img src="<?php echo base_url();?>assets/images/placeholder-images-image_large.png" class="popular-categories">
            <img src="<?php echo base_url();?>assets/images/placeholder-images-image_large.png" class="popular-categories">
          </div>
        </div>
      </div>
      <div class="row justify-content-center" style="background-color: grey;">
        <div class="col-md-auto">
          <div class="d-flex flex-wrap">
            <img src="<?php echo base_url();?>assets/images/placeholder-images-image_large.png" class="popular-categories">
            <img src="<?php echo base_url();?>assets/images/placeholder-images-image_large.png" class="popular-categories">
            <img src="<?php echo base_url();?>assets/images/placeholder-images-image_large.png" class="popular-categories">
            <img src="<?php echo base_url();?>assets/images/placeholder-images-image_large.png" class="popular-categories">
          </div>
        </div>
      </div>
    </div>
      
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="<?php echo base_url();?>assets/js/jquery-3.4.1.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/jquery.cookie-1.4.1.min.js"></script>
    <script>
    $(window).on("scroll", function() {
        $.cookie("tempScrollTop", $(window).scrollTop());
      });
      $(function() {
        if ($.cookie("tempScrollTop")) {
          $(window).scrollTop($.cookie("tempScrollTop"));
        }
      });
    </script>
  </body>
</html>