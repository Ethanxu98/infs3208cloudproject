<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/stylesheet.css">
    <title>List an item</title>
  </head>
  <body>
    <div class="container-fluid" id="main">
        <header>
        <?php include 'header.php';
        ?>
        </header>

      <div class="row justify-content-center pt-5">
        <div class="col-lg-6">
            <form method="POST" id="item-upload">
                <div class="row justify-content-center">
                                <div class="col-lg-6 form-group">
                                    <label for="sale-type">Item format:</label>
                                    <select class="form-control" name="sale-type" id="sale-type">
                                        <option class="dropdown-item" value="Sales">Fixed-price Sale</option>
                                        <option class="dropdown-item" value="Auction">Auction</option>
                                    </select>
                                </div>
                                <div class="col-lg-6 form-group">
                                    <label for="sale-duration">Item duration:</label>
                                    <select class="form-control" name="sale-duration" id="sale-duration">
                                        <option class="dropdown-item" value="1">24 hours</option>
                                        <option class="dropdown-item" value="2">2 days</option>
                                        <option class="dropdown-item" value="3">3 days</option>
                                        <option class="dropdown-item" value="5">5 days</option>
                                        <option class="dropdown-item" value="7">7 days</option>
                                        <option class="dropdown-item" value="0">Until Sold</option>
                                    </select>
                                </div>
                </div>
                <div class="row justify-content-center">
                    <div class="col-lg-6 form-group">
                        <label for="saleCategory">Item Category:</label>
                        <select class="form-control" name="saleCategory" id="saleCategory">
                            <option disabled selected value> -- Select a category -- </option>
                            <option class="dropdown-item" value="Antiques">Antiques</option>
                            <option class="dropdown-item" value="Art">Art</option>
                            <option class="dropdown-item" value="Clothing Fashion">Clothing & Fashion</option>
                            <option class="dropdown-item" value="Digital Entertainment">Digital Entertainment</option>
                            <option class="dropdown-item" value="Electronics">Electronics</option>
                            <option class="dropdown-item" value="Home Appliances">Home Appliances</option>
                            <option class="dropdown-item" value="Toys Hobbies">Toys & Hobbies</option>
                            <option class="dropdown-item" value="Sporting Outdoors">Sporting & Outdoors</option>
                            <option class="dropdown-item" value="Other">Other</option>
                        </select>
                    </div>
                    <div class="col-lg-6 form-group">
                        <label for="saleSubCategory">Subcategory:</label>
                        <select class="form-control" name="saleSubCategory" id="saleSubCategory">
                            <option disabled selected value> -- Subcategory -- </option>
                        </select>
                    </div>    
                </div>
                <div class="row justify-content-center form-group">
                    <div class="col-lg-6">
                        <label for="saleName">Item Name:</label>
                        <input class="form-control" name="saleName" id="saleName">
                        </input>
                    </div>
                    <div class="col-lg-6">
                        <label for="saleCondition">Item Condition:</label>
                        <select class="form-control" name="saleCondition" id="saleCondition">
                            <option class="dropdown-item" value="New">New</option>
                            <option class="dropdown-item" value="Used">Used</option>
                            <option class="dropdown-item" value="Opened">Opened - Not Used</option>
                        </select>
                    </div>
                </div>
                <div class="row justify-content-center">
                    <div class="col-lg-6">
                        <div class="row justify-content-center">
                            <div class="col-3 pt-4 pl-4">
                                <label for="salePrice" style="font-size: 30px;">$</label>
                            </div>
                            <div class="col-9">
                                <label for="salePrice">Item Price:</label>
                                <input class="form-control" name="salePrice" id="salePrice">
                                </input>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <label for="saleCondition">Item Quantity:</label>
                        <input class="form-control" name="saleQuantity" id="saleQuantity">
                        </input>
                    </div>
                </div>
                <div class="row-fluid justify-content-left pt-4">
                    <label for="saleDescription">Item Description:</label>
                    <textarea class="form-control" rows="10" name="saleDescription" id="saleDescription">
                    </textarea>
                </div>
                <input type="hidden" name="imageValue1" id="imageValue1" value="">
                <input type="hidden" name="imageValue2" id="imageValue2" value="">
                <input type="hidden" name="imageValue3" id="imageValue3" value="">
                <input type="hidden" name="imageValue4" id="imageValue4" value="">
                <input type="hidden" name="imageValue5" id="imageValue5" value="">
                <input type="hidden" name="imageValue6" id="imageValue6" value="">
            </form>
            <form method="POST" id="uploadImage" name="uploadImage" enctype="multipart/form-data" ondrop="dropTest(event)">
                <div class="row justify-content-center">
                    <div class="col-lg-8 form-group pt-5">
                        <div id="demo" class="carousel slide" data-ride="carousel" data-interval="false">

                            <!-- Indicators -->
                            <ul class="carousel-indicators">
                            <li data-target="#demo" data-slide-to="0" class="active"></li>
                            <li data-target="#demo" data-slide-to="1"></li>
                            <li data-target="#demo" data-slide-to="2"></li>
                            <li data-target="#demo" data-slide-to="3"></li>
                            <li data-target="#demo" data-slide-to="4"></li>
                            <li data-target="#demo" data-slide-to="5"></li>
                            </ul>

                            <!-- The slideshow -->
                            <div class="carousel-inner">
                            <div class="carousel-item active" id="saleImageSrc1">
                                <img src="<?php echo base_url();?>assets/images/placeholder-images-image_large.png" alt="Los Angeles" class="img-fluid center-block">
                            </div>
                            <div class="carousel-item" id="saleImageSrc2">
                                <img src="<?php echo base_url();?>assets/images/placeholder-images-image_large.png" alt="Chicago" class="img-fluid center-block">
                            </div>
                            <div class="carousel-item" id="saleImageSrc3">
                                <img src="<?php echo base_url();?>assets/images/placeholder-images-image_large.png" alt="New York" class="img-fluid center-block">
                            </div>
                            <div class="carousel-item" id="saleImageSrc4">
                                <img src="<?php echo base_url();?>assets/images/placeholder-images-image_large.png" class="img-fluid center-block">
                            </div>
                            <div class="carousel-item" id="saleImageSrc5">
                                <img src="<?php echo base_url();?>assets/images/placeholder-images-image_large.png" class="img-fluid center-block">
                            </div>
                            <div class="carousel-item" id="saleImageSrc6">
                                <img src="<?php echo base_url();?>assets/images/placeholder-images-image_large.png" class="img-fluid center-block">
                            </div>
                            </div>

                            <!-- Left and right controls -->
                            <a class="carousel-control-prev" href="#demo" data-slide="prev">
                            <span class="carousel-control-prev-icon"></span>
                            </a>
                            <a class="carousel-control-next" href="#demo" data-slide="next">
                            <span class="carousel-control-next-icon"></span>
                            </a>

                        </div>
                    </div>
                    <div class="col-lg-4 form-group pt-5">
                        <div class="row-fluid justify-content-center pt-5">
                            <p>You may upload a total of 6 images</p>  
                            <input type="file" name="image_file" id="image_file" /></br></br>  
                            <button name="upload" id="upload" value="Upload" onclick="test1();" class="btn btn-primary">Upload</button>
           
                        </div>
                        
                    </div>  
                </div>
            </form>
            <input type="submit" class="btn btn-primary mt-4" name="createListing" id="createListing" value="Create Listing">
        </div>
      </div>
      <div class="row justify-content-center">
        
        
      </div>
      <div class="row justify-content-center pt-5">
        <h2>Recently viewed items</h2>
      </div>
      <div class="row justify-content-center" style="background-color: grey;">
        <div class="col-md-auto">
          <div class="d-flex flex-wrap">
            <img src="<?php echo base_url();?>assets/images/placeholder-images-image_large.png" class="popular-categories">
            <img src="<?php echo base_url();?>assets/images/placeholder-images-image_large.png" class="popular-categories">
            <img src="<?php echo base_url();?>assets/images/placeholder-images-image_large.png" class="popular-categories">
            <img src="<?php echo base_url();?>assets/images/placeholder-images-image_large.png" class="popular-categories">
          </div>
        </div>
      </div>
    </div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="<?php echo base_url();?>assets/js/jquery-3.4.1.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    <script src="<?php echo base_url();?>assets/js/jquery.cookie-1.4.1.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url();?>assets/js/sellScript.js"></script>
    <script type="text/javascript">
        function dropTest(event){
            event.preventDefault(); 
            if($('#image_file').val() == '')
                {  
                    alert("Please Select the File");  
                } 
                else  
                {  
                    $.ajax({  
                        url:"<?php echo base_url();?>upload/ajax_upload",
                        method:"POST",  
                        data:new FormData(this),  
                        contentType: false,  
                        cache: false,  
                        processData:false,  
                        success:function(data)  
                            {
                                var customRegex = /\".*?\"/
                                var customData = data.match(/\".*?\"/);
                                if (document.getElementById("saleImageSrc1").classList.contains("active")){
                                    document.getElementById("saleImageSrc1").innerHTML = data;
                                    document.getElementById("imageValue1").setAttribute("value", customData[0].replace(/\"/g,""));
                                } else if (document.getElementById("saleImageSrc2").classList.contains("active")){
                                    document.getElementById("saleImageSrc2").innerHTML = data;
                                    document.getElementById("imageValue2").setAttribute("value", customData[0].replace(/\"/g,""));
                                } else if (document.getElementById("saleImageSrc3").classList.contains("active")){
                                    document.getElementById("saleImageSrc3").innerHTML = data;
                                    document.getElementById("imageValue3").setAttribute("value", customData[0].replace(/\"/g,""));
                                } else if (document.getElementById("saleImageSrc4").classList.contains("active")){
                                    document.getElementById("saleImageSrc4").innerHTML = data;
                                    document.getElementById("imageValue4").setAttribute("value", customData[0].replace(/\"/g,""));
                                } else if (document.getElementById("saleImageSrc5").classList.contains("active")){
                                    document.getElementById("saleImageSrc5").innerHTML = data;
                                    document.getElementById("imageValue5").setAttribute("value", customData[0].replace(/\"/g,""));
                                } else if (document.getElementById("saleImageSrc6").classList.contains("active")){
                                    document.getElementById("saleImageSrc6").innerHTML = data;
                                    document.getElementById("imageValue6").setAttribute("value", customData[0].replace(/\"/g,""));
                                } else { }          
                            }  
                    });  
                }
        }

        $("#uploadImage").on('submit', function(event) { 
            event.preventDefault(); 
            if($('#image_file').val() == '')
                {  
                    alert("Please Select the File");  
                } 
                else  
                {  
                    $.ajax({  
                        url:"<?php echo base_url();?>upload/ajax_upload",
                        method:"POST",  
                        data:new FormData(this),  
                        contentType: false,  
                        cache: false,  
                        processData:false,  
                        success:function(data)  
                            {
                                var customRegex = /\".*?\"/
                                var customData = data.match(/\".*?\"/);
                                if (document.getElementById("saleImageSrc1").classList.contains("active")){
                                    document.getElementById("saleImageSrc1").innerHTML = data;
                                    document.getElementById("imageValue1").setAttribute("value", customData[0].replace(/\"/g,""));
                                } else if (document.getElementById("saleImageSrc2").classList.contains("active")){
                                    document.getElementById("saleImageSrc2").innerHTML = data;
                                    document.getElementById("imageValue2").setAttribute("value", customData[0].replace(/\"/g,""));
                                } else if (document.getElementById("saleImageSrc3").classList.contains("active")){
                                    document.getElementById("saleImageSrc3").innerHTML = data;
                                    document.getElementById("imageValue3").setAttribute("value", customData[0].replace(/\"/g,""));
                                } else if (document.getElementById("saleImageSrc4").classList.contains("active")){
                                    document.getElementById("saleImageSrc4").innerHTML = data;
                                    document.getElementById("imageValue4").setAttribute("value", customData[0].replace(/\"/g,""));
                                } else if (document.getElementById("saleImageSrc5").classList.contains("active")){
                                    document.getElementById("saleImageSrc5").innerHTML = data;
                                    document.getElementById("imageValue5").setAttribute("value", customData[0].replace(/\"/g,""));
                                } else if (document.getElementById("saleImageSrc6").classList.contains("active")){
                                    document.getElementById("saleImageSrc6").innerHTML = data;
                                    document.getElementById("imageValue6").setAttribute("value", customData[0].replace(/\"/g,""));
                                } else { }          
                            }  
                    });  
                }
        }); 
        
        function test2(){
            document.forms['item-upload'].action = '<?php echo base_url();?>sell/listItemValidation';
            document.forms['item-upload'].submit();
        }
        
        $(window).on("scroll", function() {
            $.cookie("tempScrollTop", $(window).scrollTop());
        });
        $(function() {
            if ($.cookie("tempScrollTop")) {
                $(window).scrollTop($.cookie("tempScrollTop"));
            }
        });
        
    </script>
  </body>
</html>