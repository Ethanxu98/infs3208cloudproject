<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/stylesheet.css">
    <title><?php print_r($itemResult->saleName);?></title>
  </head>
  <body>
    <header>
      <?php include 'header.php';?>
    </header>
    <div class="container-fluid" id="main">
      <div class="row justify-content-center pt-5" style="text-align:center;">
        <div class="col-lg-3">
          <div class="card" style="max-width:100%">
            <div id="demo" class="carousel slide" data-ride="carousel" data-interval="false">
              <!-- Indicators -->
              <ul class="carousel-indicators">
                <li data-target="#demo" data-slide-to="0" class="active"></li>
                <li data-target="#demo" data-slide-to="1"></li>
                <li data-target="#demo" data-slide-to="2"></li>
                <li data-target="#demo" data-slide-to="3"></li>
                <li data-target="#demo" data-slide-to="4"></li>
                <li data-target="#demo" data-slide-to="5"></li>
              </ul>
              <!-- The slideshow -->
              <div class="carousel-inner" id="itemDetailCarousel">
              <?php
              $imageGallery = array();
              if ($itemResult->saleImage1 != ''){
                $imageGallery[0] = $itemResult->saleImage1;
              } else {
                $imageGallery[0] = base_url().'assets/images/placeholder-images-image_large.png';
              } 
              if ($itemResult->saleImage2 != ''){
                $imageGallery[1] = $itemResult->saleImage2;
              } else {
                $imageGallery[1] = base_url().'assets/images/placeholder-images-image_large.png';
              }
              if ($itemResult->saleImage3 != ''){
                $imageGallery[2] = $itemResult->saleImage3;
              } else {
                $imageGallery[2] = base_url().'assets/images/placeholder-images-image_large.png';
              }
              if ($itemResult->saleImage4 != ''){
                $imageGallery[3] = $itemResult->saleImage4;
              } else {
                $imageGallery[3] = base_url().'assets/images/placeholder-images-image_large.png';
              }
              if ($itemResult->saleImage5 != ''){
                $imageGallery[4] = $itemResult->saleImage5;
              } else {
                $imageGallery[4] = base_url().'assets/images/placeholder-images-image_large.png';
              }
              if ($itemResult->saleImage6 != ''){
                $imageGallery[5] = $itemResult->saleImage6;
              } else {
                $imageGallery[5] = base_url().'assets/images/placeholder-images-image_large.png';
              }
              ?>
                <div class="carousel-item active">
                  <img src="<?php print_r($imageGallery[0]); ?>" alt="Los Angeles" class="img-fluid center-block" id="itemImageCarouselSrc1">
                </div>
                <div class="carousel-item">
                  <img src="<?php print_r($imageGallery[1]); ?>" alt="Chicago" class="img-fluid center-block" id="itemImageCarouselSrc2">
                </div>
                <div class="carousel-item">
                  <img src="<?php print_r($imageGallery[2]); ?>" alt="New York" class="img-fluid center-block" id="itemImageCarouselSrc3">
                </div>
                <div class="carousel-item">
                  <img src="<?php print_r($imageGallery[3]); ?>" alt="New York" class="img-fluid center-block" id="itemImageCarouselSrc4">
                </div>
                <div class="carousel-item">
                  <img src="<?php print_r($imageGallery[4]); ?>" alt="New York" class="img-fluid center-block" id="itemImageCarouselSrc5">
                </div>
                <div class="carousel-item">
                  <img src="<?php print_r($imageGallery[5]); ?>" alt="New York" class="img-fluid center-block" id="itemImageCarouselSrc6">
                </div>
              </div>
              <!-- Left and right controls -->
              <a class="carousel-control-prev" href="#demo" data-slide="prev">
                <span class="carousel-control-prev-icon"></span>
              </a>
              <a class="carousel-control-next" href="#demo" data-slide="next">
                <span class="carousel-control-next-icon"></span>
              </a>
            </div>
            <div class="card-body">
              <h2>Item Image Gallery</h2>
            </div>
          </div>
        </div>
        <div class="col-lg-3">
          <div class="d-flex flex-column justify-content-between" style="height:85%;">
            <h2 id="itemTitleName"><?php print_r($itemResult->saleName);?></h2>
            <div class="d-flex justify-content-center">
              <p><?php print_r($itemResult->saleCategory);?>></p>
              <p><?php print_r($itemResult->saleSubCategory);?></p>
            </div>
            <p id="itemDescription"><?php print_r($itemResult->saleItemDescription);?></P>
            <?php
            '<select class="form-control" id="specialItemOption">
              <option class="dropdown-item">Option 1</option>
              <option class="dropdown-item">Option 2</option>
              <option class="dropdown-item">Option 3</option>
            </select>'
            ?>
          </div>
        </div>
        <div class="col-lg-3 col-xl-2">
          <div class="row d-flex flex-column flex-wrap justify-content-start border align-items-center" style="height:40%; margin-bottom:2%;">
            <p id="sellerName">Seller Name</p>
            <div class="d-flex flex-row justify-content-center">
              <img src="<?php echo base_url();?>assets/images/star.png" alt="Seller rating" style="max-width:20px; max-height:20px;" class="sellerStarRatingIcon">
              <img src="<?php echo base_url();?>assets/images/star.png" alt="Seller rating" style="max-width:20px; max-height:20px;" class="sellerStarRatingIcon">
              <img src="<?php echo base_url();?>assets/images/star.png" alt="Seller rating" style="max-width:20px; max-height:20px;" class="sellerStarRatingIcon">
              <img src="<?php echo base_url();?>assets/images/star.png" alt="Seller rating" style="max-width:20px; max-height:20px;" class="sellerStarRatingIcon">
              <img src="<?php echo base_url();?>assets/images/star.png" alt="Seller rating" style="max-width:20px; max-height:20px;" class="sellerStarRatingIcon">
              <p id="sellerRatingPercentage">100%</p>
            </div>
            <a href="#" class="btn btn-outline-primary" style="width:60%;">Contact Seller</a>
            <a href="#" class="btn btn-outline-primary" style="width:60%; margin-top:2%;">Sellers' Items</a>
          </div>
          <div class="row d-flex flex-column justify-content-start border align-items-center" style="height:58%;">
            <div class="d-flex flex-row justify-content-center">
              <h2>$</h2>
              <h2 id="itemPrice"><?php print_r($itemResult->salePrice);?></h2> 
            </div>
            <p>Quantity:</p>
            <div class="input-group d-flex flex-row justify-content-center mb-4" style="width:60%;">
              <span class="input-group-btn">
                <button type="button" class="quantity-left-minus btn btn-number"  data-type="minus" data-field="">
                  <img src="<?php echo base_url();?>assets/images/minus.png" style="width:20px; height:20px;">
                </button>
              </span>
              <input type="text" id="quantity" name="itemQuantity" class="form-control input-number" value="1" min="1" max="100">
                <span class="input-group-btn">
                  <button type="button" class="quantity-right-plus btn btn-number" data-type="plus" data-field="">
                    <img src="<?php echo base_url();?>assets/images/plus.png" style="width:20px; height:20px;">
                  </button>
                </span>
            </div>
            <a href="button" class="btn btn-primary mb-2">Add to Cart</a>
            <?php 
            $currentUser = $this->session->userdata('userID');
              if (isset($itemResult->userIDFav) && isset($itemResult->itemIDFav)){
                echo '<a href="'.base_url().'item/removeFromFavourite/'.$itemResult->saleID.'/'.$currentUser.'" class="btn btn-primary">Remove from Watch List</a>';
              }
              else {
                echo '<a href="'.base_url().'item/addToFavourite/'.$itemResult->saleID.'" class="btn btn-primary">Add to Watch List</a>';
              }
            ?>
            
          </div>
        </div>    
      </div>

      <div class="containter">
        <div class="row justify-content-center pt-5">
          <div class="col-lg-8">  
            <ul class="nav nav-tabs" role="tablist">
              <li class="nav-item">
                <a class="nav-link active" data-toggle="tab" href="#home">Specifications</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#menu1">Postage</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#menu2">Reviews & Ratings</a>
              </li>
            </ul>
        
            <div class="row-fluid justify-content-center border" style="height:50vh;">
      
              <!-- Tab panes -->
              <div class="tab-content pt-5">
                <div class="tab-pane container active" id="home">
                  <p><?php print_r($itemResult->saleItemDescription);?></p>
                </div>
                <div class="tab-pane container fade" id="menu1">

                </div>
                <div class="tab-pane container fade" id="menu2">
                  <?php echo form_open('item/review/'.$itemResult->saleID.'');?>
                  <div class="d-flex flex-row justify-content-center ratings">
                    <p>How would you rate this product?</p>
                    <label class="pl-3 pr-3 active">
                      <input type="radio" class="form-check-input ratingRadio" name="ratings" style="opacity:0;" value="1">
                      <img src="<?php echo base_url();?>assets/images/starGray.png" alt="Seller rating" style="max-width:20px; max-height:20px;cursor:pointer;" class="hoverStar" onmouseover="hoverStar(0)">
                    </label>
                    <label class="pl-3 pr-3">
                      <input type="radio" class="form-check-input ratingRadio" name="ratings" style="opacity:0;" value="2">
                      <img src="<?php echo base_url();?>assets/images/starGray.png" alt="Seller rating" style="max-width:20px; max-height:20px;cursor:pointer;" class="hoverStar" onmouseover="hoverStar(1)">
                    </label>
                    <label class="pl-3 pr-3">
                      <input type="radio" class="form-check-input ratingRadio" name="ratings" style="opacity:0;" value="3">
                      <img src="<?php echo base_url();?>assets/images/starGray.png" alt="Seller rating" style="max-width:20px; max-height:20px;cursor:pointer;" class="hoverStar" onmouseover="hoverStar(2)">
                    </label>
                    <label class="pl-3 pr-3">
                      <input type="radio" class="form-check-input ratingRadio" name="ratings" style="opacity:0;" value="4">
                      <img src="<?php echo base_url();?>assets/images/starGray.png" alt="Seller rating" style="max-width:20px; max-height:20px;cursor:pointer;" class="hoverStar" onmouseover="hoverStar(3)">
                    </label>
                    <label class="pl-3 pr-3">
                      <input type="radio" class="form-check-input ratingRadio" name="ratings" style="opacity:0;" value="5">
                      <img src="<?php echo base_url();?>assets/images/starGray.png" alt="Seller rating" style="max-width:20px; max-height:20px;cursor:pointer;" class="hoverStar" onmouseover="hoverStar(4)">
                    </label>
                  </div>
                  <div class="form-group d-flex flex-column mt-5 justify-content-center align-items-center">
                    <label for="reviewComment">Write a review on this product:</label>
                    <textarea id="reviewComment" name="reviewComment" class="text" rows="6" style="width:80%;"></textarea>
                  </div>
                  <input type="submit" class="btn btn-primary" value="Submit Review">
                  </form>
                  
                  <div class="row-fluid pt-5">
                    <p>All user reviews</p>
                    <?php foreach($reviewResults as $row): ?>
                      <?php 
                      if ($row->reviewRating == 1){
                        echo
                        '<div class="d-flex flex-row">
                          <p class="mr-4">'.$row->userFName.$row->reviewerID.': <br></p>
                          <img src="'.base_url().'assets/images/star.png" alt="Seller rating" style="max-width:20px; max-height:20px;cursor:pointer;">
                          <p class="ml-4">'.$row->reviewComment.'</p>
                        </div>';
                      } else if ($row->reviewRating == 2){
                        echo
                        '<div class="d-flex flex-row">
                          <p class="mr-4">'.$row->userFName.$row->reviewerID.': <br></p>
                          <img src="'.base_url().'assets/images/star.png" alt="Seller rating" style="max-width:20px; max-height:20px;cursor:pointer;">
                          <img src="'.base_url().'assets/images/star.png" alt="Seller rating" style="max-width:20px; max-height:20px;cursor:pointer;">
                          <p class="ml-4">'.$row->reviewComment.'</p>
                        </div>';
                      } else if ($row->reviewRating == 3){
                        echo
                        '<div class="d-flex flex-row">
                          <p class="mr-4">'.$row->userFName.$row->reviewerID.': <br></p>
                          <img src="'.base_url().'assets/images/star.png" alt="Seller rating" style="max-width:20px; max-height:20px;cursor:pointer;">
                          <img src="'.base_url().'assets/images/star.png" alt="Seller rating" style="max-width:20px; max-height:20px;cursor:pointer;">
                          <img src="'.base_url().'assets/images/star.png" alt="Seller rating" style="max-width:20px; max-height:20px;cursor:pointer;">
                          <p class="ml-4">'.$row->reviewComment.'</p>
                        </div>';
                      } else if ($row->reviewRating == 4){
                        echo
                        '<div class="d-flex flex-row">
                          <p class="mr-4">'.$row->userFName.$row->reviewerID.': <br></p>
                          <img src="'.base_url().'assets/images/star.png" alt="Seller rating" style="max-width:20px; max-height:20px;cursor:pointer;">
                          <img src="'.base_url().'assets/images/star.png" alt="Seller rating" style="max-width:20px; max-height:20px;cursor:pointer;">
                          <img src="'.base_url().'assets/images/star.png" alt="Seller rating" style="max-width:20px; max-height:20px;cursor:pointer;">
                          <img src="'.base_url().'assets/images/star.png" alt="Seller rating" style="max-width:20px; max-height:20px;cursor:pointer;">
                          <p class="ml-4">'.$row->reviewComment.'</p>
                        </div>';
                      } else {
                        echo
                        '<div class="d-flex flex-row">
                          <p class="mr-4">'.$row->userFName.$row->reviewerID.': <br></p>
                          <img src="'.base_url().'assets/images/star.png" alt="Seller rating" style="max-width:20px; max-height:20px;cursor:pointer;">
                          <img src="'.base_url().'assets/images/star.png" alt="Seller rating" style="max-width:20px; max-height:20px;cursor:pointer;">
                          <img src="'.base_url().'assets/images/star.png" alt="Seller rating" style="max-width:20px; max-height:20px;cursor:pointer;">
                          <img src="'.base_url().'assets/images/star.png" alt="Seller rating" style="max-width:20px; max-height:20px;cursor:pointer;">
                          <img src="'.base_url().'assets/images/star.png" alt="Seller rating" style="max-width:20px; max-height:20px;cursor:pointer;">
                          <p class="ml-4">'.$row->reviewComment.'</p>
                        </div>';
                      }
                      ?>
                    <?php endforeach;?>
                  </div>
                </div>
              </div>
          
            </div>
          </div>
        </div>
      </div>

      <div class="row justify-content-center pt-5">
        <h2>Recently viewed items</h2>
      </div>
      <div class="row justify-content-center" style="background-color: grey;">
        <div class="col-md-auto">
          <div class="d-flex flex-wrap">
            <img src="img/" class="popular-categories">
            <img src="img/placeholder-images-image_large.png" class="popular-categories">
            <img src="img/placeholder-images-image_large.png" class="popular-categories">
            <img src="img/placeholder-images-image_large.png" class="popular-categories">
          </div>
        </div>
      </div>
    </div>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    <script src="<?php echo base_url();?>assets/js/jquery-3.4.1.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/jquery.countdown.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/jquery.cookie-1.4.1.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/itemScript.js"></script>
    <script>
      function hoverStar(imgNum){
        if (imgNum == 0){
          document.getElementsByClassName("ratings")[0].getElementsByTagName("img")[imgNum].src = "<?php echo base_url();?>assets/images/star.png";
        } else if (imgNum == 1){
          document.getElementsByClassName("ratings")[0].getElementsByTagName("img")[imgNum-1].src = "<?php echo base_url();?>assets/images/star.png";
          document.getElementsByClassName("ratings")[0].getElementsByTagName("img")[imgNum].src = "<?php echo base_url();?>assets/images/star.png";
        } else if (imgNum == 2){
          document.getElementsByClassName("ratings")[0].getElementsByTagName("img")[imgNum-2].src = "<?php echo base_url();?>assets/images/star.png";
          document.getElementsByClassName("ratings")[0].getElementsByTagName("img")[imgNum-1].src = "<?php echo base_url();?>assets/images/star.png";
          document.getElementsByClassName("ratings")[0].getElementsByTagName("img")[imgNum].src = "<?php echo base_url();?>assets/images/star.png";
        } else if (imgNum == 3){
          document.getElementsByClassName("ratings")[0].getElementsByTagName("img")[imgNum-3].src = "<?php echo base_url();?>assets/images/star.png";
          document.getElementsByClassName("ratings")[0].getElementsByTagName("img")[imgNum-2].src = "<?php echo base_url();?>assets/images/star.png";
          document.getElementsByClassName("ratings")[0].getElementsByTagName("img")[imgNum-1].src = "<?php echo base_url();?>assets/images/star.png";
          document.getElementsByClassName("ratings")[0].getElementsByTagName("img")[imgNum].src = "<?php echo base_url();?>assets/images/star.png";
        } else if (imgNum == 4){
          document.getElementsByClassName("ratings")[0].getElementsByTagName("img")[imgNum-4].src = "<?php echo base_url();?>assets/images/star.png";
          document.getElementsByClassName("ratings")[0].getElementsByTagName("img")[imgNum-3].src = "<?php echo base_url();?>assets/images/star.png";
          document.getElementsByClassName("ratings")[0].getElementsByTagName("img")[imgNum-2].src = "<?php echo base_url();?>assets/images/star.png";
          document.getElementsByClassName("ratings")[0].getElementsByTagName("img")[imgNum-1].src = "<?php echo base_url();?>assets/images/star.png";
          document.getElementsByClassName("ratings")[0].getElementsByTagName("img")[imgNum].src = "<?php echo base_url();?>assets/images/star.png";
        }
      }
      $("#timer").countdown("<?php print_r($itemDurationLeft->timeLeft);?>", function(event){
        $("#timer").text(event.strftime('Duration: %-D days %-H hours %-M minutes %-S'));
      });
    </script>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <!-- Icons made by <a href="https://www.flaticon.com/authors/becris" title="Becris">Becris</a> from <a href="https://www.flaticon.com/" title="Flaticon"> www.flaticon.com</a> -->
  </body>
</html>