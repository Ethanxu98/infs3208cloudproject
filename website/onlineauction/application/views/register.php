<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/stylesheet.css">
    <title>Register</title>
  </head>
  <body>
    <header>
      <?php
        include 'header.php';
      ?>
    </header>
    <div class="container-fluid" id="main">
        <div class="row justify-content-center" style="padding-top: 5%;">
          <h2>New Customer</h2>
        </div>
        <div class="row justify-content-center">
          <div class="col-4">
            <form method="POST" action="<?php echo base_url();?>register/validation" id="register-form">
              <div class="form-group">
                <label for="email">Email address:</label>
                <input type="email" class="form-control" placeholder="Enter email" id="email" name="email" value="<?php echo set_value('email');?>">
                <span class="text-danger"><?php echo form_error('email');?></span>
              </div>
              <div class="form-group">
                <label for="pwd">Password:</label>
                <input type="password" class="form-control" placeholder="Enter password" id="pwd" name="pwd">
                <span class="text-danger"><?php echo form_error('pwd');?></span>
              </div>
              <div class="row justify-content-center">
                <div class="col-lg-6">
                  <div class="form-group">
                    <label for="username-firstname">First Name</label>
                    <input type="username-firstname" class="form-control" placeholder="Name" id="userFName" name="userFName" value="<?php echo set_value('userFName');?>">
                    <span class="text-danger"><?php echo form_error('userFName');?></span>
                  </div>
                </div>
                <div class="col-lg-6">
                  <div class="form-group">
                    <label for="username-surname">Last Name</label>
                    <input type="username-surname" class="form-control" placeholder="Name" id="userLName" name="userLName" value="<?php echo set_value('userLName');?>">
                    <span class="text-danger"><?php echo form_error('userLName');?></span>
                  </div>
                </div>
              </div>
              <div class="form-group">
                <label for="address1">Street Address:</label>
                <input type="address" class="form-control" placeholder="Address Line 1" id="address1" name="address1">
                <span class="text-danger"><?php echo form_error('address1');?></span>
              </div>
              <div class="form-group">
                <label for="address2">Address Line 2</label>
                <input type="address" class="form-control" placeholder="Address Line 2" id="address2" name="address2">
              </div>
              <div class="row justify-content-center">
                <div class="col-lg-6">
                  <div class="form-group">
                    <label for="city">City/Suburb</label>
                    <input type="city" class="form-control" placeholder="Name" id="city" name="city" value="<?php echo set_value('userFName');?>">
                    <span class="text-danger"><?php echo form_error('city');?></span>
                  </div>
                </div>
                <div class="col-lg-6">
                  <div class="form-group">
                    <label for="state">State/Province</label>
                    <input type="state" class="form-control" placeholder="Name" id="state" name="state" value="<?php echo set_value('userLName');?>">
                    <span class="text-danger"><?php echo form_error('state');?></span>
                  </div>
                </div>
              </div>
              <div class="row justify-content-center">
                <div class="col-lg-6">
                  <div class="form-group">
                    <label for="postcode">Postcode</label>
                    <input type="postcode" class="form-control" placeholder="Postcode" id="postcode" name="postcode" value="<?php echo set_value('userFName');?>">
                  </div>
                </div>
                <div class="col-lg-6">
                  <div class="form-group">
                    <label for="country">Country</label>
                    <input type="country" class="form-control" placeholder="Name" id="country" name="country" value="<?php echo set_value('userLName');?>">
                    <span class="text-danger"><?php echo form_error('country');?></span>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="form-group form-check" style="text-align: left; width: 50%;">
                  <p>Existing user? <a href="<?php echo base_url();?>login">Login</a></p>
                </div>
              </div>
              <button type="submit" class="btn btn-primary" name="register">Submit</button>
            </form>
          </div>
        </div>
    </div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    
  </body>
</html>