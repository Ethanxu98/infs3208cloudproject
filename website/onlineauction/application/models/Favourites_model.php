<?php
class Favourites_model extends CI_Model{
    function getFavouriteItem($userID){
        $query = $this->db->query(
			"SELECT * FROM userfavourites, salesitem WHERE userfavourites.itemIDFav=salesitem.saleID AND userIDFav=$userID"
        );
        return $query;
    }
    function addFavouriteItem($data){
        $this->db->insert('userfavourites', $data);
        return;
    }
    function removeFavouriteItem($id, $currentUser){
        $this->db->query("DELETE FROM userfavourites WHERE userIDFav=$currentUser AND itemIDFav=$id");
        return;
    }
}
?>