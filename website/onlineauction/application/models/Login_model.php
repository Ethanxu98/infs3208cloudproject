<?php
class Login_model extends CI_Model{
    function can_login($email, $password){
        $this->load->helper('cookie');
        $this->db->where('userEmail', $email);
        $query = $this->db->get('users');
        
        if ($query->num_rows() > 0){
            foreach($query->result() as $row){
                $store_password = $row->userPwd;
                $query1 = $this->db->query("SELECT userVerified FROM users WHERE userEmail='$email'")->row()->userVerified;
                if ($query1 == 1){
                    if (password_verify($password, $store_password)){
                        $newdata = array(
                            'userID' => $row->userID,
                            'userFName' => $row->userFName,
                            'userLName' => $row->userLName,
                            'email' => $row->userEmail,
                            'logged_in' => TRUE
                        );
                        $this->session->set_userdata($newdata);
                        if ($this->input->post('autoLogin'))
                        {
                            $this->input->set_cookie('rememberedEmail', $email, 86500); 
                            $this->input->set_cookie('rememberedPwd', $password, 86500);
                        }
                        else
                        {
                            delete_cookie('rememberedEmail');
                            delete_cookie('rememberedPwd');
                        }
                    }
                    else{
                        return 'Wrong Password';
                    }
                } else {
                    return 'Your Email Address has not yet being verified, please check your email and verify it.';
                }
            }
        }
        else {
            return 'Wrong Email Address';
        }
    }
    function emailExist($email){
        $sql = "SELECT userFName, userEmail FROM users WHERE userEmail='$email' LIMIT 1";
        $result = $this->db->query($sql);
        $row = $result->row();
        return ($result->num_rows() == 1 && $row->userEmail)? $row->userFName : FALSE;
    }
    function verifyResetPasswordCode($email, $code){
        $sql = "SELECT userFName, userEmail FROM users WHERE userEmail='$email' LIMIT 1";
        $result = $this->db->query($sql);
        $row = $result->row();

        if ($result->num_rows() === 1){
            return ($code == md5($this->config->item('salt').$row->userFName))? True : False;
        } else {
            return false;
        }
    }
    function updatePassword(){
        $email = $this->input->post('email');
        $encrypted_password = password_hash($this->input->post('pwd'), PASSWORD_BCRYPT);
        $sql = "UPDATE users SET userPwd = '$encrypted_password' WHERE userEmail = '$email'";
        $this->db->query($sql);
        $this->session->set_flashdata('message','Successfully reset password!');
        redirect('login');
        $this->session->flashdata('message');
        return ($this->db->affected_rows() > 0);
    }
}
?>