<?php
class Item_model extends CI_Model{
    function getItemDetails($id){
        $querys = $this->db->query("SELECT * FROM salesitem WHERE saleID=$id");
        return $querys;
    }
    function getItemFavDetails($user, $item){
        $query = $this->db->query("SELECT * FROM userfavourites WHERE userIDFav=$user AND itemIDFav=$item");
        return $query;
    }
    function getMaxBid($id){
        $query = $this->db->query("SELECT MAX(bidAmount) as maxBid FROM salesitem, itembids WHERE saleID=bidItemID AND saleID=$id");
        return $query;
        
    }
    function getCurrentUserBid($id, $user){
        $query = $this->db->query("SELECT bidItemID, bidBuyerID, bidAmount FROM itembids, salesitem WHERE bidItemID=saleID AND (bidItemID=$id AND bidBuyerID=$user) AND bidAmount=(SELECT MAX(bidAmount) FROM itembids, salesitem WHERE bidItemID=saleID AND (saleID=$id AND bidBuyerID=$user))");
        return $query;
    }
    function noBid($id){
        $query = $this->db->query("SELECT * FROM salesitem WHERE saleID=$id");
        return $query;
    }
}
?>