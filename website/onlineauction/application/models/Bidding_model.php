<?php
class Bidding_model extends CI_Model{
    function insertBid($itemID, $amount, $bidderID, $sellerID){
        $data = array(
            'bidID' => NULL,
            'bidItemID' => $itemID,
            'bidAmount' => $amount,
            'bidBuyerID' => $bidderID,
            'bidSellerID' => $sellerID,
            'bidPostage' => 'Express',
            'bidPostageCost' => 50,

        );
        $this->db->insert('itembids', $data);
        return;
    }
    function finishBid($item, $winnerID, $seller, $price){
        if (($winnerID == 0 || $winnerID == NULL) && ($price == 0 || $price == NULL)){
            $query = $this->db->query("UPDATE salesitem SET saleActive=0 WHERE saleID=$item");
            return;
        } else {
            $query = $this->db->query("UPDATE salesitem SET saleActive=0 WHERE saleID=$item");
            $query2 = $this->db->query("INSERT INTO itemorders (`orderID`, `orderCost`, `orderBuyerID`, `orderSellerID`, `orderItemID`, `orderPostage`, `orderPostageCost`) VALUES (NULL, '$price', '$winnerID', '$seller', '$item', 'Express', '50')");
            return;
        }
    }
    function getWinningBid($id){
        $query = $this->db->query("SELECT bidItemID, bidBuyerID, bidAmount FROM itembids, salesitem WHERE bidItemID=saleID AND saleID=$id AND bidAmount=(SELECT MAX(bidAmount) bidAmount FROM itembids, salesitem WHERE bidItemID=saleID AND saleID=$id)")->row();
        if (isset($query->bidAmount)){
            return $query;
        } else {
            return 0;
        }
        
    }
}
?>