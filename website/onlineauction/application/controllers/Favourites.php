<?php
class Favourites extends CI_Controller{
    public function index(){
        $this->load->model('favourites_model');
        $currentUser = $this->session->userdata('userID');
        $data['favourite'] = $this->favourites_model->getFavouriteItem($currentUser);
        $this->load->view('favourites', $data);
    }
    function favouriteItemID($id){
        $this->load->model("Item_model");
        $id = $this->uri->segment(3);
        $itemResult['itemResult'] = $this->Item_model->getItemDetails($id)->row();
        $this->load->view('item_details.php', $itemResult);
    }
    function removeFromFavourite($id, $currentUser){
        $id = $this->uri->segment(3);
        $currentUser = $this->session->userdata('userID');
        $this->load->model('favourites_model');
        $this->favourites_model->removeFavouriteItem($id, $currentUser);
        redirect('favourites');
    }
}
?>