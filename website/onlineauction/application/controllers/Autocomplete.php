<?php

class Autocomplete extends CI_Controller{
    
    public function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('autocomplete_model');
    }
    function index(){
    }

    function getAutocomplete(){
        $searchTerm = $this->input->get('searchTerm');
        if (isset($term)) {
            $result = $this->autocomplete_model->searchItem($searchTerm);
            if (count($result) > 0) {
                foreach ($result as $row){
                    $array_result[] = $row->saleName;
                    echo json_encode($array_result);
                }
            }
            return $result;
        }
    }
}

?>