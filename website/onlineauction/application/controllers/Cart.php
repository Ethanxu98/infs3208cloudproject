<?php
class Cart extends CI_Controller{
    public function __construct(){
        parent::__construct();
        $this->load->model('Item_model');
        $this->load->model('Cart_model');
    }
    public function index(){
        $currentUser = $this->session->userdata('userID');
        $data['cart'] = $this->Cart_model->getShoppingCart($currentUser);
        $this->load->view('cart', $data);
    }
    function favouriteItemID($id){
        $id = $this->uri->segment(3);
        $itemResult['itemResult'] = $this->Item_model->getItemDetails($id)->row();
        $this->load->view('item_details.php', $itemResult);
    }
    function removeFromFavourite($id, $currentUser){
        $id = $this->uri->segment(3);
        $currentUser = $this->session->userdata('userID');
        $this->Cart_model->removeFavouriteItem($id, $currentUser);
        redirect('favourites');
    }
}
?>