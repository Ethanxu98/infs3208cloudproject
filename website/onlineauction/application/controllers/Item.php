<?php
class Item extends CI_Controller {
    public function __construct(){
        parent::__construct();
        $this->load->library('session');
        $this->load->library('form_validation');
        $this->load->model('Item_model');
        $this->load->model('favourites_model');
        $this->load->model('Bidding_model');
        $this->load->model('Review_model');
    }
    public function index(){
    }
    function itemID($id){
        $currentUser = $this->session->userdata('userID');
        $id = $this->uri->segment(3);
        $itemDurationLeft = $this->db->query("SELECT DATE_FORMAT(saleEnd, '%Y/%m/%d %H:%i:%s') AS timeLeft FROM salesitem WHERE saleID=$id")->row();
        $itemDuration = $this->db->query("SELECT TIME_TO_SEC(TIMEDIFF(saleEnd, NOW())) timeDuration FROM salesitem WHERE saleID=$id")->row();
        $userMaxBid = $this->Item_model->getCurrentUserBid($id, $currentUser)->row();
        $data = array(
            'itemResult' => $this->Item_model->getItemDetails($id)->row(),
            'itemBidResult' => $this->Item_model->getMaxBid($id)->row(),
            'reviewResults' => $this->Review_model->getReviews($id)->result(),
            'itemDurationLeft' => $itemDurationLeft,
            'itemDurationLeftTick' => $itemDuration,
            'userMaxBid' => $userMaxBid
        );
        $itemType = $this->db->query("SELECT * FROM salesitem WHERE saleID=$id")->row();
        if ($itemType->saleType == 'Sales'){
            $this->load->view('item_details.php', $data);
        } else {
            $this->load->view('item_detailsBid.php', $data);
            if ($itemDuration->timeDuration <= 0 && $itemType->saleActive == 1){
                $currentUserMaxBid = $this->Bidding_model->getWinningBid($id);
                if (isset($currentUserMaxBid->bidBuyerID)){
                    $this->Bidding_model->finishBid($id, $currentUserMaxBid->bidBuyerID, $itemType->sellerID, $currentUserMaxBid->bidAmount);
                    $data2 = array(
                        'itemResult' => $this->Item_model->getItemDetails($id)->row(),
                        'itemBidResult' => $this->Item_model->getMaxBid($id)->row(),
                        'reviewResults' => $this->Review_model->getReviews($id)->result(),
                        'itemDurationLeft' => $itemDuration,
                        'winningBid' => $currentUserMaxBid
                    );
                    $this->load->view('item_detailsBid.php', $data2);
                } else {
                    $this->Bidding_model->finishBid($id, 0, $itemType->sellerID, 0);
                    $this->load->view('item_detailsBid.php', $data);
                }
            }
        }
    }
    function addToFavourite($id){
        $id = $this->uri->segment(3);
        $currentUser = $this->session->userdata('userID');
        $data = array(
            'userIDFav' => $currentUser,
            'itemIDFav' => $id
        );
        if ($currentUser == '' || $currentUser == NULL){
            redirect('login');
        } else {
            $this->favourites_model->addFavouriteItem($data);
            redirect('item/itemID/'.$id.'');
        }
    }
    function removeFromFavourite($id, $currentUser){
        $id = $this->uri->segment(3);
        $currentUser = $this->session->userdata('userID');
        $this->favourites_model->removeFavouriteItem($id, $currentUser);
        redirect('item/itemID/'.$id.'');
    }
    function bid($itemID, $bidderID, $sellerID){
        $amount = $this->input->post('bidInput');
        $query = $this->db->query("SELECT MAX(bidAmount) AS maxBid FROM itembids WHERE bidItemID=$itemID")->row()->maxBid;
        $query2 = $this->db->query("SELECT salePrice FROM salesitem WHERE saleID=$itemID")->row()->salePrice;

        if ($query < 100){

            if ($amount >= $query+2 && $amount >= $query2){
                $itemID = $this->uri->segment(3);
                $bidderID = $this->uri->segment(4);
                $sellerID = $this->uri->segment(5);
                $this->Bidding_model->insertBid($itemID, $amount, $bidderID, $sellerID);
                redirect("item/itemID/".$itemID."");
            }
            else if ($amount < $query2){
                $this->session->set_flashdata('message', 'You cannot bid below starting price');
                redirect("item/itemID/".$itemID."");
            }
            else {
                $this->session->set_flashdata('message', 'You cannot bid below minimum bid');
                redirect("item/itemID/".$itemID."");
            }
        } else if ($query >= 100 && $query < 500){
            if ($amount >= $query+10 && $amount >= $query2){
                $itemID = $this->uri->segment(3);
                $bidderID = $this->uri->segment(4);
                $sellerID = $this->uri->segment(5);
                $this->Bidding_model->insertBid($itemID, $amount, $bidderID, $sellerID);
                redirect("item/itemID/".$itemID."");
            }
            else if ($amount < $query2){
                $this->session->set_flashdata('message', 'You cannot bid below starting price');
                redirect("item/itemID/".$itemID."");
            }
            else {
                $this->session->set_flashdata('message', 'You cannot bid below minimum bid');
                redirect("item/itemID/".$itemID."");
            }
        } else {
            if ($amount >= $query+20 && $amount >= $query2){
                $itemID = $this->uri->segment(3);
                $bidderID = $this->uri->segment(4);
                $sellerID = $this->uri->segment(5);
                $this->Bidding_model->insertBid($itemID, $amount, $bidderID, $sellerID);
                redirect("item/itemID/".$itemID."");
            }
            else if ($amount < $query2){
                $this->session->set_flashdata('message', 'You cannot bid below starting price');
                redirect("item/itemID/".$itemID."");
            }
            else {
                $this->session->set_flashdata('message', 'You cannot bid below minimum bid');
                redirect("item/itemID/".$itemID."");
            }
        }
    }
    function review($itemID){
        $itemID = $this->uri->segment(3);
        $reviewerID = $this->session->userdata('userID');
        $sellerID = $this->db->query("SELECT sellerID FROM salesitem WHERE saleID=$itemID")->row()->sellerID;
        $rating = $this->input->post('ratings');
        $comment = $this->input->post('reviewComment');
        $anonymous = $_POST['anonPost'];
        if (isset($anonymous)){
            $anonymous = TRUE;
            $this->Review_model->writeReview($itemID, $reviewerID, $sellerID, $rating, $comment, $anonymous);
            $this->session->set_flashdata('message', 'Review posted');
            redirect("item/itemID/".$itemID."");
        } else {
            $anonymous = FALSE;
            $this->Review_model->writeReview($itemID, $reviewerID, $sellerID, $rating, $comment, $anonymous);
            $this->session->set_flashdata('message', 'Review posted');
            redirect("item/itemID/".$itemID."");
        }
        
        
    }
}
?>