<?php
class Sell extends CI_Controller {
    public function __construct(){
        parent::__construct();
        $this->load->library('session');
        $this->load->library('form_validation');
        $this->load->model('Sell_model');
    }
    function index(){
        $this->load->view('sell.php');
    }
    function listItemValidation(){
        $this->form_validation->set_rules('saleCategory', 'category','required|trim');
        $this->form_validation->set_rules('saleSubCategory', 'subcategory','required|trim');
        $this->form_validation->set_rules('saleName', 'name','required|trim');
        $this->form_validation->set_rules('salePrice', 'price','required|trim');
        $this->form_validation->set_rules('saleQuantity', 'quantity','required|trim');
        $this->form_validation->set_rules('saleDescription', 'description','required|trim');
        if($this->form_validation->run()){
            $timestamp = date('Y-m-d H:i:s');
            $duration = $this->input->post('sale-duration');

            $sellerID = $this->session->userdata('userID');
            $saleType = $this->input->post('sale-type');
            $saleDuration = $this->input->post('sale-duration');
            $saleActive = TRUE;
            $saleCategory = $this->input->post('saleCategory');
            $saleStart = ''.$timestamp.'';
            $saleEnd = 'NOW() + INTERVAL '.$duration.' DAY';
            $saleSubCategory = $this->input->post('saleSubCategory');
            $saleCondition = $this->input->post('saleCondition');
            $saleName = $this->input->post('saleName');
            $salePrice = $this->input->post('salePrice');
            $saleQuantity = $this->input->post('saleQuantity');
            $saleItemDescription = $this->input->post('saleDescription');
            $saleImage1 = $this->input->post('imageValue1');
            $saleImage2 = $this->input->post('imageValue2');
            $saleImage3 = $this->input->post('imageValue3');
            $saleImage4 = $this->input->post('imageValue4');
            $saleImage5 = $this->input->post('imageValue5');
            $saleImage6 = $this->input->post('imageValue6');
            
            
            $id = $this->Sell_model->insert($saleType, $saleName, $sellerID, $salePrice, $saleStart, $saleEnd, $saleActive, $saleDuration, $saleCategory, $saleSubCategory, $saleCondition, $saleImage1, $saleImage2, $saleImage3, $saleImage4, $saleImage5, $saleImage6, $saleItemDescription, $saleQuantity);
            $this->session->set_flashdata('message','Item listed successfully!');
            redirect('sell');
            
            
            
        }
        else {
            $this->index();
        }
    }
}

?>