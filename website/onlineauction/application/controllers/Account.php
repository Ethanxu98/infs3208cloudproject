<?php
class Account extends CI_Controller {

    public function index(){
        $this->load->model('account_model');
        $accDetails['accDetails'] = $this->account_model->accountDetails()->row();
        $this->load->view('user_profile.php', $accDetails);
        
    }
    function updateAccountInfo(){
        
        $this->load->model('account_model');
        $this->account_model->update();
    }
}