<?php

class Home extends CI_Controller {

	public function index()
	{
		$this->load->database();
		$this->load->view('index.php');
	}
	
	public function userLogin()
	{
		$this->load->view('login_page.php');
	}
	public function register(){
		$this->load->view('register.php');
	}
	public function logout(){
		$data = $this->session->sess_destroy();
		redirect('home');
	}
	public function sell(){
		$this->load->view('sell.php');
	}
	public function search(){
		$this->load->model('search_model');
		$searchTerm = $this->input->get('searchTerm');
		$category = $this->input->get('categorySearch');
		$searchData['searchData'] = $this->search_model->searchItem($searchTerm, $category);
		$this->load->view('category_items.php', $searchData);
	}
}
?>