<?php
class Login extends CI_Controller
{
    public function __construct(){
        parent::__construct();
        $this->load->library('session');
        $this->load->library('form_validation');
        $this->load->library('encryption');
        $this->load->library('email');
        $this->load->helper('cookie');
        $this->load->model('login_model');
    }
    function index(){
        $this->load->view('login_page');
    }
    function validation(){
        $this->form_validation->set_rules('email', 'email address', 'required|trim|valid_email');
        $this->form_validation->set_rules('pwd','password','required');
        if($this->form_validation->run()){
            $result = $this->login_model->can_login($this->input->post('email'),$this->input->post('pwd'));
            if($result == ''){
                $loggedInUser = $this->session->userdata('userFName');
                redirect('home');
            } else {
                $this->session->set_flashdata('message', $result);
                redirect('login');
            }
        }
        else {
            $this->index();
        }
    }
    function resetPassword(){
        if (isset($_POST['email']) && !empty($_POST['email'])){
            $this->form_validation->set_rules('email', 'Email Address', 'trim|required|valid_email');

            if ($this->form_validation->run() == False){
                $this->session->set_flashdata('message', 'Please enter a valid email.');
                redirect('login/resetPassword');
            } else {
                $email = trim($this->input->post('email'));
                $emailExist = $this->login_model->emailExist($email);

                if ($emailExist) {
                    $this->sendResetPassword($email, $emailExist);
                    redirect('login/resetPassword');
                } else {
                    $this->session->set_flashdata('message','The email you entered does not exist.');
                    redirect('login/resetPassword');
                }
            }
        } else {
            $this->load->view('forgot_password');
        }
        
    }
    function sendResetPassword($email, $userFName){
        $emailCode = md5($this->config->item('salt').$userFName);
        $config = array(
            'protocol' => 'smtp',
            'smtp_host' => 'mailhub.eait.uq.edu.au',
            'smtp_port' => 25,
            'mailtype' => 'html',
            'charset' => 'iso-8859-1',
            'wordwrap' => TRUE
        );
        $this->email->initialize($config);
        $this->email->from('noreply@infs3202-97180a7e.uqcloud.net');
        $this->email->to($email);
        $this->email->subject('Reset your password at Online Auction');

        $message = '<!DOCTYPE html PUBLIC " -//W3C//DTD XHTML 1.0 Strict//EN"
                    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"><html>
                    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
                    </head><body>';
        $message .= '<p>Dear ' .$userFName.',</p>';
        $message .= '<p>You have requested to reset your password at Online Auction. Please <strong><a href="'.base_url().'login/resetPasswordForm/'.$email.'/'.$emailCode.'">click here</a></strong> to reset your password.</p>';
        $message .= '<p>Thanks</p>';
        $message .= '<p>Online Auctions</p>';
        $message .= '</body></html>';

        $this->email->message($message);
        $this->email->send();
        if (! $this->email->send()){
            $this->session->set_flashdata('message','Email successfully sent!');
        } else {
            $this->session->set_flashdata('message','Something went wrong, try again later.');
        }
    }
    function resetPasswordForm($email, $emailCode){
        if (isset($email, $emailCode)){
            $email = trim($email);
            $emailHash = sha1($email.$emailCode);
            $verified = $this->login_model->verifyResetPasswordCode($email, $emailCode);

            if ($verified){
                $this->load->view('update_password', array('emailHash' => $emailHash, 'emailCode' => $emailCode, 'email' => $email));
            } else {
                $this->load->view('forgot_password', array('error' => 'There was a problem with the token. Please request again to reset your password'));
            }
        }
    }
    function updatePassword(){
        if (!isset($_POST['email'], $_POST['emailHash']) || $_POST['emailHash'] !== sha1($_POST['email'].$_POST['emailCode'])){
            die('Error updating your password');
        }

        $this->form_validation->set_rules('emailHash', 'Email Hash','trim|required');
        $this->form_validation->set_rules('pwd', 'password','required');
        if ($this->form_validation->run()){
            $this->login_model->updatePassword();
        } else {
            redirect('update_password');

        }
    }
}
