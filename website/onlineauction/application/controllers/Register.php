<?php
class Register extends CI_Controller
{
    public function __construct(){
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->library('encryption');
        $this->load->library('email');
        $this->load->model('register_model');
    }
    function index(){
        $this->load->view('register');
    }
    function validation(){
        $this->form_validation->set_message('is_unique', 'This %s is already taken');
        $this->form_validation->set_rules('userFName', 'name','required|trim');
        $this->form_validation->set_rules('userLName', 'name','required|trim');
        $this->form_validation->set_rules('email', 'email address','required|trim|valid_email|is_unique[users.userEmail]');
        $this->form_validation->set_rules('pwd', 'password','required|matches[pwd]|min_length[8]|max_length[32]|callback_password_check');
        $this->form_validation->set_rules('address1', 'address','required|trim');
        $this->form_validation->set_rules('city', 'city/suburb name','required|trim');
        $this->form_validation->set_rules('state', 'state/province name','required|trim');
        $this->form_validation->set_rules('country', 'name','required|trim');
        if($this->form_validation->run()){
            
            $encrypted_password = password_hash($this->input->post('pwd'), PASSWORD_BCRYPT);
            $emailVar = $this->input->post('email');
            $emailVarF = $this->input->post('userFName');
            $data = array (
                'userFName' => $this->input->post('userFName'),
                'userLName' => $this->input->post('userLName'),
                'userEmail' => $this->input->post('email'),
                'userPwd' => $encrypted_password,
                'userVerified' => TRUE
            );
            $addressData = array (
                'emailID' => $this->input->post('email'),
                'StreetAddress' => $this->input->post('address1'),
                'AddressLine2' => $this->input->post('address2'),
                'City' => $this->input->post('city'),
                'State' => $this->input->post('state'),
                'PostCode' => $this->input->post('postcode'),
                'Country' => $this->input->post('country')
            );
            $id = $this->register_model->insert($data);
            $id2 = $this->register_model->insertAddress($addressData);
            //$this->emailVerification($emailVar, $emailVarF);
            $this->session->set_flashdata('message','Successfully registered! You must verify your email first before you may login using your email and password');
            redirect('login');
        }
        else {
            $this->index();
        }

    }
/*
    function emailVerification($email, $name){
        $verificationToken = sha1($email.$name);
        $config = array(
            'protocol' => 'smtp',
            'smtp_host' => 'mailhub.eait.uq.edu.au',
            'smtp_port' => 25,
            'mailtype' => 'html',
            'charset' => 'iso-8859-1',
            'wordwrap' => TRUE
        );
        $this->email->initialize($config);
        $this->email->from('noreply@infs3202-97180a7e.uqcloud.net');
        $this->email->to($email);
        $this->email->subject('Please verify your email at Online Auctions');

        $message = '<!DOCTYPE html PUBLIC " -//W3C//DTD XHTML 1.0 Strict//EN"
                    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"><html>
                    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
                    </head><body>';
        $message .= '<p>Dear ' .$name.',</p>';
        $message .= '<p>Welcome to Online Auctions! To get started please <strong><a href="'.base_url().'register/emailVerify/'.$email.'/'.$verificationToken.'">click here</a></strong> to verify your email address. You must verify your email address before being able to login.</p>';
        $message .= '<p>Thanks</p>';
        $message .= '<p>Online Auctions</p>';
        $message .= '</body></html>';

        $this->email->message($message);
        $this->email->send();
    }
    function emailVerify($email, $token){
        $this->register_model->verifyEmail($email, $token);
        $this->session->set_flashdata('message','Email Verified! You may now login using your email and password.');
        redirect('login');
    }
 */
    public function password_check($str){
        if (preg_match('#[0-9]#', $str) && preg_match('#[a-zA-Z]#', $str)) {
            return TRUE;
        } else {
            
            return FALSE;
        }
        
    }
}
