<?php  
 defined('BASEPATH') OR exit('No direct script access allowed');  
 class Upload extends CI_Controller {  
      //functions
     public function index(){
          $this->load->view('sell.php');
     }
      function ajax_upload()  
      {  
           if(isset($_FILES["image_file"]["name"]))  
           {  
                $config['upload_path'] = './assets/images/';  
                $config['allowed_types'] = 'jpg|jpeg|png|gif';  
                $this->load->library('upload', $config);  
                if(!$this->upload->do_upload('image_file'))  
                {  
                     echo $this->upload->display_errors();  
                }  
                else  
                {  
                     $data = $this->upload->data();  
                     echo '<img src="'.base_url().'assets/images/'.$data["file_name"].'" class="img-thumbnail" />';
                } 
           }  
     }
 }