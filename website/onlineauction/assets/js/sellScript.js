$(document).ready(function(){
   console.log(document.getElementsByClassName('carousel-item').item(0).innerHTML);
   
   console.log(document.querySelector('.carousel-item'));

   function test1(){
      document.forms['uploadImage'].submit();
   }
   document.getElementById("createListing").addEventListener("click", test2, false);
   
   
     // Map your choices to your option value
   var subCategoryChoice = {
      'Antiques': ['Architectural', 'Decorative Arts', 'Furniture','Silver','Sculptures'],
      'Art': ['Indigenous', 'Drawings','Photographs','Paintings'],
      'Clothing Fashion': ['Mens\' Fashion','Womens\' Fashion', 'Childrens\' Fashion', 'Accessories', 'Costumes'],
      'Digital Entertainment': ['Video Games','DVD, Movies','CD, Music', 'Books & Magazines'],
      'Electronics': ['Pre-built Computers','Computer Components','Smartphones','Cameras','Network Hardware','Drones','Tablets','Audio Equipment','TVs & Monitors','Video Game Consoles'],
      'Home Appliances': ['Furniture','Kitchen','Lighting','Gardening','Home Decor','DIY & Materials'],
      'Toys Hobbies': ['Action Figures','Childrens\' Toys','Collectible Card Games','Model Toys','Puzzles','Outdoor Toys'],
      'Sporting Outdoors': ['Sports Equipment','Camping, Hiking','Fishing','Fitness'],
      'Other': ['Miscellaneous'],
   }

// When an option is changed, search the above for matching choices
   $('#saleCategory').on('change', function() {
      // Set selected option as variable
      var selectValue = $(this).val();

      // Empty the target field
      $('#saleSubCategory').empty();
      
      // For each chocie in the selected option
      for (i = 0; i < subCategoryChoice[selectValue].length; i++) {
         // Output choice in the target field
         $('#saleSubCategory').append("<option value='" + subCategoryChoice[selectValue][i] + "'>" + subCategoryChoice[selectValue][i] + "</option>");
      }
   });

   $('#salePrice').on('keypress', function (event) {
      var regex = new RegExp("^[0-9\.?]+$");
      var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
      if (!regex.test(key)) {
         event.preventDefault();
         return false;
      }
   });

   $('#saleQuantity').on('keypress', function (event) {
      var regex = new RegExp("^[0-9]+$");
      var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
      if (!regex.test(key)) {
         event.preventDefault();
         return false;
      }
   });



});
