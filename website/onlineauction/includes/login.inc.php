<?php
if (isset($_POST['login-submit'])){
    require 'databaseHandler.inc.php';

    $userEmail = $_POST['email'];
    $userPwd = $_POST['pwd'];

    if (empty($userEmail) || empty($userPwd)){
        header("Location: ../login_page.php?error=emptyfields");
        exit();
    }
    else {
        $sql = "SELECT * FROM users WHERE userEmail=?";
        $stmt = mysqli_stmt_init($conn);
        if (!mysqli_stmt_prepare($stmt, $sql)) {
            header("Location: ../login_page.php?error");
            exit();
        }
        else {
            mysqli_stmt_bind_param($stmt, "s", $userEmail);
            mysqli_stmt_execute($stmt);
            $result = mysqli_stmt_get_result($stmt);
            if ($row = mysqli_fetch_assoc($result)) {
                $pwdCheck = password_verify($userPwd, $row['userPwd']);
                if ($pwdCheck == false){
                    header("Location: ../login_page.php?error");
                    exit();
                }
                else if ($pwdCheck == true){
                    session_start();
                    $_SESSION['userID'] = $row['userEmail'];
                    header("Location: ../index.php?login=success");
                    exit();

                }
                else {
                    header("Location: ../login_page.php?error");
                    exit();
                }
            }
            else {
                header("Location: ../login_page.php?error");
                exit();
            }
        }

    }
}
else {
    header("Location: ../login_page.php");
    exit();
}