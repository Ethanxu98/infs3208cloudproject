<?php
if (isset($_POST['register-submit'])){
    require 'databaseHandler.inc.php';

    $userEmail = $_POST['email'];
    $userFName = $_POST['userFName'];
    $userLName = $_POST['userLName'];
    $userPassword = $_POST['pwd'];

    if (empty($userEmail) || empty($userFName) || empty($userLName) || empty($userPassword)){
        header("Location: ../register.php?error=emptyfields&email=".$userEmail."&userFName=".$userFName."&userLName=".$userLName);
        exit();
    }
    else {
        $sql = "SELECT userEmail FROM users WHERE userEmail=?";
        $stmt = mysqli_stmt_init($conn);

        if(!mysqli_stmt_prepare($stmt, $sql)){
            header("Location: ../register.php?error=sqlerror");
            exit();
        }
        else {
            mysqli_stmt_bind_param($stmt, "s", $userEmail);
            mysqli_stmt_execute($stmt);
            mysqli_stmt_store_result($stmt);
            $resultCheck = mysqli_stmt_num_rows($stmt);

            if ($resultCheck > 0){
                header("Location: ../register.php?error=userEmailTaken&userFName=".$userFName."&userLName=".$userLName);
                exit();
            }
            else {
                $sql = "INSERT INTO users (userEmail, userFName, userLName, userPwd) VALUES (?, ?, ?, ?)";
                $stmt = mysqli_stmt_init($conn);

                if (!mysqli_stmt_prepare($stmt, $sql)){
                    header("Location: ../register.php?error=userEmailTaken&userFName=".$userFName."&userLName=".$userLName);
                    exit();
                }
                else {
                    $hashedPwd = password_hash($userPassword, PASSWORD_DEFAULT);

                    mysqli_stmt_bind_param($stmt, "ssss", $userEmail, $userFName, $userLName, $hashedPwd);
                    mysqli_stmt_execute($stmt);
                    header("Location: ../register.php?signup=success");
                    exit();
                }
            }
        }
    }
    mysqli_stmt_close($stmt);
    mysqli_close($conn);
}
else {
    header("Location: ../register.php");
    exit();
}